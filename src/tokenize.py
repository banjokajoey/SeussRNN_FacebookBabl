import sys
import vocabulary as voc


def tokenize_file(in_path, out_path):
    print ('tokeniziing file...')

    # Open out file
    out_file = open(out_path, 'w')

    with open(in_path, 'r') as in_file:
        for line in in_file:
            # Get line and clean it
            line = line.strip()
            if line[0] == '_':
                continue
            line = line.lower()

            # Tokenize
            tokens = line.split()
            out_file.write('{0} {1} {2}\n '.format(voc.START_TOKEN, ' '.join(tokens), voc.END_TOKEN))

    # Close out file
    out_file.close()

    print ('wrote text to file: {0}'.format(out_path))


if __name__ == '__main__':
    in_filepath = 'cbt/data/cbt_test.txt' if len(sys.argv) < 2 else sys.argv[1]
    out_filepath = 'seuss_rnn/data/sample.txt' if len(sys.argv) < 3 else sys.argv[2]
    tokenize_file(in_filepath, out_filepath)