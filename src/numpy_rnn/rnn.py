import math
import numpy as np
import utils


# RNN Class

class RecurrentNeuralNet(object):

    # Constructor

    def __init__(self, size):
        # State
        self.size = size
        self.learning_rate = 0.01
        self.max_history = 1
        self.__reset_history()

        # Weights
        size_root = math.sqrt(size)
        self.weights_in = np.random.rand(size, size) / size_root
        self.weights_hidden = np.random.rand(size, size) / size_root
        self.weights_predict = np.random.rand(size, size) / size_root

        # Weight Gradients
        self.weight_grads_in = np.empty((size, size))
        self.weight_grads_hidden = np.empty((size, size))
        self.weight_grads_predict = np.empty((size, size))

        # Convenience arrays make sure they are PARALLEL
        self.weight_mats = [self.weights_in, self.weights_hidden, self.weights_predict]
        self.weight_grads = [self.weight_grads_in, self.weight_grads_hidden, self.weight_grads_predict]

        # Regularization
        self.l2_strength = 0.0


    # Predict:
    #   - uses current state to predict what vector should
    #     should come after the passed in vector
    # TODO:
    #   - consider not copying this array for speedup purposes.


    def predict(self, in_vec):
        prediction = self.__forward(in_vec)
        self.last_prediction = np.copy(prediction)
        return prediction


    # Generate:
    # Input:
    #   - seed_vec: what vector to generate from. This
    #       vector is included in the returned sequence.
    #   - stop_val: when the RNN outputs a vector whose argmax
    #       is equal to this value, it will stop generating.
    #   - max_len: early stop
    # Output:
    #   - a python list of generated numpy vectors


    def generate_sequence(self, seed_vec, stop_val, max_len=5):
        # Free memory
        self.__reset_history()

        # Generate sequence
        seq = [seed_vec]
        last_vec = seed_vec
        while True:
            next_vec = self.predict(last_vec)
            seq.append(next_vec)
            if np.argmax(next_vec) == stop_val or len(seq) >= max_len:
                break
            last_vec = next_vec
        return seq


    # Train:
    #   - accepts sequence of vectors as input
    #   - accumulates weight gradients on a per vector basis
    #   - performs one weight update at the end of the sequence
    # How to:
    #   - To train RNN on a corpus, pass whole sequences to this
    #     function one at a time.

    def train(self, vec_sequence):
        # Free memory
        self.__free_old_history()

        # Assume the sequence starts with the proper seed.
        seed = vec_sequence[0]

        # Accumulate gradients with each vec in the sequence
        for i, vec in enumerate(vec_sequence):
            label = vec_sequence[i + 1] if i < (len(vec_sequence) - 1) else seed
            predictions = self.predict(vec)
            loss = self.__calculate_loss(predictions, label)
            loss_grads = loss * self.__calculate_loss_drv(predictions, label)
            
            # Weight update
            self.__clear_gradients()
            self.__backward_through_time(loss_grads, self.max_history)
            self.__weight_update()


    # Forward prop:
    #   - updates hidden state
    #   - returns vector

    def __forward(self, in_vec):
        # Store input for BPTT later
        self.inputs.append(in_vec)

        # Calculate current state
        state = np.tanh(np.dot(self.weights_in, in_vec) + self.state_hist[-1])

        # Softmax out from state. Store softmax input for BPTT later
        self.predict_in = np.dot(self.weights_predict, state)
        predict_out = utils.softmax(self.predict_in)

        # Transform current state and save it as the next hidden state
        self.state_hist.append(np.dot(self.weights_hidden, state))

        # Return softmax out
        return predict_out


    # Back prop:
    #   - accumulates weight gradients

    def __backward_through_time(self, loss_grads, iters=0):
        # Backprop through softmax
        softmax_drv = utils.softmax_drv(self.last_prediction)
        predict_grads_in = np.dot(softmax_drv, loss_grads)
        self.weight_grads_predict = utils.jacobian(self.predict_in, predict_grads_in)

        # Backprop through time
        state_grads_out = utils.from_jacobian(self.weight_grads_predict)
        iters = min(iters, len(self.state_hist) - 1)
        for i in range(iters):
            # Remember RNN state
            in_vec = self.inputs[-1 - i]
            state_vec = self.state_hist[-1 - i]
            hidden_vec = self.state_hist[-1 - i - 1]
            
            # Backprop loss across tanh activation
            state_grads_in = state_grads_out * utils.tanh_drv(state_vec)

            # Accumulate weights
            self.weight_grads_in += utils.jacobian(in_vec, state_grads_in)
            self.weight_grads_hidden += utils.jacobian(hidden_vec, state_grads_in)
            
            # Move back in time
            state_grads_out = utils.from_jacobian(self.weight_grads_hidden)


    # Weights


    def __weight_update(self):
        for i, weight_mat in enumerate(self.weight_mats):
            # Weight update
            weight_mat -= self.learning_rate * self.weight_grads[i]

            # L2 regularization
            weight_mat *= 1 - self.l2_strength


    def __weights_squared_sum(self):
        # For L2 regularization:
        # (0.5 * l2_strength * self.__weights_squared_sum())
        sq_sum = 0
        for weight_mat in self.weight_mats:
            sq_sum += np.sum(np.square(weight_mat))
        return sq_sum


    # Loss

    def __calculate_loss(self, predictions, label):
        # Cross entropy loss
        loss = self.__cross_entropy(predictions, label)

        # Add L2 regularization
        loss += 0.5 * self.l2_strength * self.__weights_squared_sum()

        # Return
        return loss


    def __calculate_loss_drv(self, predictions, label):
        return self.__cross_entropy_drv(predictions, label)


    def __cross_entropy(self, predictions, label):
        return -1 * np.sum(np.log(predictions) * label)


    def __cross_entropy_drv(self, predictions, label):
        return -1 * np.reciprocal(predictions) * label


    # Memory management

    def __reset_history(self):
        self.inputs = []
        self.state_hist = [np.zeros(self.size)]


    def __free_old_history(self):
        if len(self.inputs) > 50 * self.max_history:
            self.inputs = self.inputs[:-1 * self.max_history]

        if len(self.state_hist) > 50 * (self.max_history + 1):
            self.state_hist = self.state_hist[:-1 * self.max_history - 1]


    def __clear_gradients(self):
        self.weight_grads_in.fill(0.0)
        self.weight_grads_hidden.fill(0.0)
        self.weight_grads_predict.fill(0.0)


    # Print

    def pprint(self):
        print ('===============================')
        print ('RNN')
        print ('-------------------------------')
        print ('size: {0}'.format(self.size))
        print ('hidden: {0}'.format(self.state_hist[-1]))
        print ('weights_in:\n{0}'.format(self.weights_in))
        print ('weights_hidden:\n{0}'.format(self.weights_hidden))
        print ('weights_predict:\n{0}'.format(self.weights_predict))


# Script handle

if __name__ == '__main__':
    print ('\nChecking if RNN runs without crashing...')

    net_size = 5
    rnn = RecurrentNeuralNet(net_size)

    for i in range(3):
        rnn.train([np.array([i for i in range(net_size)]), np.array([i for i in range(net_size)])])
    print ('Training complete.')

    prediction = rnn.predict(np.array([i for i in range(net_size)]))
    print ('Prediction complete: {0}'.format(prediction))

    generation = rnn.generate_sequence(0, 2, [1])
    print ('Generation complete: {0}'.format(generation))

    print ('Test complete.\n')