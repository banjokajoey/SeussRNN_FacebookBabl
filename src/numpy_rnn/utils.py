import numpy as np


def one_hot_vec(index, size, reuse_vec=None):
    vec = reuse_vec if reuse_vec is not None else np.empty(size, dtype=float)
    vec.fill(0)
    vec[index] = 1
    return vec


def softmax(in_vec):
    exp = np.exp(in_vec)
    return exp / np.sum(exp)


def softmax_drv(in_vec):
    dim = len(in_vec)
    soft = softmax(in_vec)
    identity = np.identity(dim)
    jacob_mat = np.asmatrix(np.zeros(dim * dim)).reshape(dim, dim)
    for i in range(dim):
        soft_i = soft[i]
        for j in range(dim):
            ident = identity[i][j]
            soft_j = soft[j]
            drv = soft_i * (ident - soft_j)
            jacob_mat[i, j] = drv
    return jacob_mat


def softmax_slope(in_vec):
    dim = len(in_vec)
    soft = softmax(in_vec)
    delta = 0.001
    jacob_mat = np.asmatrix(np.zeros(dim * dim)).reshape(dim, dim)
    for i in range(dim):
        in_vec[i] += delta
        new_soft = softmax(in_vec)
        for j in range(dim):
            jacob_mat[i, j] = (new_soft[j] - soft[j]) / delta
        in_vec[i] -= delta
    return jacob_mat


def tanh(in_vec):
    return np.tanh(in_vec)


def tanh_drv(in_vec):
    return np.negative(np.square(np.tanh(in_vec))) + 1


def jacobian(in_vec, out_grads):
    dim = len(in_vec)
    in_mat = np.asmatrix(in_vec).reshape(dim, 1)
    grad_mat = np.asmatrix(out_grads).reshape(1, dim)
    return np.copy(np.dot(in_mat, grad_mat))


def from_jacobian(matrix):
    # sums matrix columns
    #
    # [ [ 0, 1, 2]
    #   [ 3, 4, 5]    --> [9, 12, 15]
    #   [ 6, 7, 8] ] 
    #
    # SHOULD return one rank less but for some
    # reason always returns same rank

    return np.sum(matrix, axis=0).flatten()


