import numpy as np
import os
import utils


# Constants

CACHE_FILEPATH = '../data/vocab_cache.txt'
START_TOKEN = '<START>'
END_TOKEN = '<END>'


# Vocabulary class

class Vocabulary(object):

    # Constructor

    def __init__(self, corpus_filepath, vocab_size):
        # Try to load word list from cache file
        word_list = load_word_list_from_cache(CACHE_FILEPATH)
        if not word_list:
            # No cache, build word list from scratch
            word_list = build_word_list_from_corpus(corpus_filepath, vocab_size)
            cache_word_list(CACHE_FILEPATH, word_list)

        # Build word-to-index map
        word_to_index = {}
        for i, word in enumerate(word_list):
            word_to_index[word] = i

        # Store results in object
        self.word_list = word_list
        self.word_to_index = word_to_index


    # Public interface

    def size(self):
        # Plus one for unseen words
        return len(self.word_list) + 1


    def word_to_vec(self, word):
        index = self.__word_to_index(word)
        return utils.one_hot_vec(index, self.size())


    def words_to_vecs(self, sentence):
        vecs = []
        for word in sentence:
            vecs.append(self.word_to_vec(word))
        return np.array(vecs)


    def index_to_word(self, index):
        return self.word_list[index]


    def indices_to_words(self, indices):
        words = []
        for index in indices:
            words.append(self.index_to_word(index))
        return words


    def vec_to_word(self, vec):
        index = np.argmax(vec)
        return self.index_to_word(index)


    def vecs_to_words(self, vec_seq):
        words = []
        for vec in vec_seq:
            words.append(self.vec_to_word(vec))
        return words


    # Helpers

    def __word_to_index(self, word):
        if word not in self.word_to_index:
            return len(self.word_list)
        return self.word_to_index[word]


    def __index_to_word(self, index):
        return self.word_list[index] if index < len(self.word_list) else '<UNKNOWN>'


# Cache

def load_word_list_from_cache(cache_filepath):
    if not os.path.isfile(cache_filepath):
        print ('no vocab cache found at: {0}'.format(cache_filepath))
        return None

    print ('loading vocab cache from: {0}'.format(cache_filepath))
    vocab = []
    with open(cache_filepath, 'r') as cache:
        for word in cache:
            word = word.strip()
            if len(word) > 0:
                vocab.append(word)
    return vocab


def cache_word_list(cache_filepath, word_list):
    print ('caching vocab at: {0}'.format(cache_filepath))
    with open(cache_filepath, 'w') as cache:
        for word in word_list:
            cache.write('{0}\n'.format(word))


# Build

def build_word_list_from_corpus(corpus_filepath, vocab_size):
    print ('building vocab of size {0} from corpus: {1}'.format(vocab_size, corpus_filepath))

    # Count words
    counter = {}
    with open(corpus_filepath, 'r') as corpus:
        for line in corpus:
            tokens = line.split()
            for token in tokens:
                token = token.strip()
                if token not in counter:
                    counter[token] = 0
                counter[token] += 1
    
    # Put all words in huge list
    counts_list = []
    for i, (word, count) in enumerate(counter.items()):
        counts_list.append((word, count))

    # Sort and truncate list
    counts_list.sort(key=lambda x: x[1], reverse=True)
    if vocab_size:
        counts_list = counts_list[:vocab_size]

    # Assemble vocab
    vocab = []
    for word, count in counts_list:
        vocab.append(word)
    return vocab
        