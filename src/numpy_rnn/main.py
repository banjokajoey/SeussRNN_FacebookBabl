import rnn as recurrent_net
import vocabulary

# Constants

CORPUS_FILE = '../data/sample.txt'
VOCAB_SIZE = 8000
VOCAB_TEST_SIZE = 100


# Main function

def main(corpus_filepath, num_sentences):
    print ('==============================\nSEUSS RNN\n------------------------------')

    print ('Getting vocabulary...')
    vocab = vocabulary.Vocabulary(corpus_filepath, VOCAB_TEST_SIZE)

    print ('\nBuilding RNN...')
    rnn = recurrent_net.RecurrentNeuralNet(vocab.size())

    print ('\nTraining RNN...')
    with open(CORPUS_FILE) as corpus:
        for line in corpus:
            tokens = line.split()
            vec_seq = vocab.words_to_vecs(tokens)
            rnn.train(vec_seq)
            break

    print ('\nGenerating {0} sentences...'.format(num_sentences))
    seed_vec = vocab.word_to_vec(vocabulary.START_TOKEN)
    for i in range(num_sentences):
        seq = rnn.generate_sequence(seed_vec, vocabulary.END_TOKEN)
        sentence = vocab.vecs_to_words(seq)
        print ('\t{0}'.format(sentence_to_str(sentence)))

    print ('\nEnd script.\n')


# Helper

def sentence_to_str(tokens):
    return ' '.join(tokens)


# Script handle

if __name__ == '__main__':
    main(CORPUS_FILE, 2)