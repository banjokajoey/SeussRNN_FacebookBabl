from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import SimpleRNN # LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
import numpy as np
import os
import vocabulary

# Constants

VOCAB_SIZE = 8000
VOCAB_TEST_SIZE = 100
WINDOW_SIZE = 4
CORPUS_FILE = '../../data/sample.txt'
WEIGHTS_DIR = '../../data/keras_lstm/'
WEIGHTS_WRITE_NAME = 'lstm_weights_{epoch:02d}_{loss:.4f}.hdf5'
MAX_WORDS = 100

TOY_CORPUS = '../../data/toy.txt'
TOY_VOCAB_SIZE = 7


# Main function

def main(corpus_filepath, vocab_size, num_sentences, weights_filepath=None):
    print ('==============================\nSEUSS RNN (keras)\n------------------------------')

    print ('Getting vocabulary...')
    vocab = vocabulary.Vocabulary(corpus_filepath, vocab_size)

    print ('\nReading corpus...')
    int_corpus = None
    with open(corpus_filepath) as corpus:
        raw_text = corpus.read()
        tokens = raw_text.split()
        int_corpus = vocab.words_to_ints(tokens)
    print ('int_corpus (first 50)\n{0}'.format(int_corpus[:50]))
    
    print ('\nCompiling training data...')
    x_data = []
    y_data = []
    for i in range(len(int_corpus) - WINDOW_SIZE):
        x_data.append(int_corpus[i : i + WINDOW_SIZE])
        y_data.append(int_corpus[i + WINDOW_SIZE])
    x_data = np.reshape(x_data, (len(x_data), WINDOW_SIZE, 1)) # (samples, window_size, num_features)
    start_x = x_data[0]
    x_data = x_data / float(vocab.size())
    y_data = np_utils.to_categorical(y_data)
    print ('compiled {0} examples {1} labels'.format(len(x_data), len(y_data)))
    print ('\nx_data (first 5):\n{0}'.format(x_data[:5]))
    print ('\ny_data (first 5):\n{0}'.format(y_data[:5]))

    print ('\nBuilding RNN...')
    rnn = Sequential()
    rnn.add(SimpleRNN(vocab.size(), input_shape=(x_data.shape[1], x_data.shape[2])))
    rnn.add(Dense(y_data.shape[1], activation='softmax'))
    rnn.compile(loss='categorical_crossentropy', optimizer='adam')
    print ('RNN built.')

    if weights_filepath:
        print ('\nLoading RNN weights from file...')
        rnn.load_weights(os.path.join(WEIGHTS_DIR, weights_filepath))
        print ('Weights loaded.')
    else:
        print ('\nTraining RNN...')
        save_weights_filepath = os.path.join(WEIGHTS_DIR, WEIGHTS_WRITE_NAME)
        save_weights_checkpoint = ModelCheckpoint(save_weights_filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
        rnn.fit(x_data, y_data, epochs=30, batch_size=128, callbacks=[save_weights_checkpoint])

    print ('\nGenerating {0} sentences...'.format(num_sentences))
    all_words = np.empty(MAX_WORDS + WINDOW_SIZE)
    all_words[:len(start_x)] = start_x.flatten()
    i_gen = 0
    sentences_left = num_sentences
    while i_gen < MAX_WORDS:
        seed = all_words[i_gen : i_gen + WINDOW_SIZE]
        seed = np.reshape(seed, (1, len(seed), 1))
        seed = seed / float(vocab.size())

        print ('\npredict from: {0}'.format(seed.flatten()))
        prediction = rnn.predict(seed, verbose=0)
        print ('prediction: {0}'.format(prediction))
        word = np.argmax(prediction)
        print ('argmax: {0}'.format(word))
        all_words[len(start_x) + i_gen] = word
        i_gen += 1
        if word == vocab.get_stop_int():
            sentences_left -= 1

    print ('\nGenerated:')
    print (all_words)
    print (vocab.ints_to_words(all_words))

    print ('\nEnd script.\n')





# Script handle

if __name__ == '__main__':
    main(TOY_CORPUS, TOY_VOCAB_SIZE, 2)#, '../../data/keras_lstm/lstm_weights_17_0.0055.hdf5')


