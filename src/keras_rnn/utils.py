def file_stats(filepath):
    tokens = set()
    num_seen = 0
    with open(filepath) as file:
        for line in file:
            line_tokens = line.split()
            tokens.update(line_tokens)
            num_seen += len(line_tokens)
    unique = len(tokens)

    print ('num_tokens: {0}'.format(num_seen))
    print ('unique:     {0}'.format(unique))
