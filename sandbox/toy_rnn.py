import sys
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))
src_path = os.path.join(dir_path[:len(dir_path) - len('sandbox')], 'src')
sys.path.append(src_path)

import numpy as np
import rnn as recurrent_net
import utils


# Script handle

if __name__ == '__main__':
    print ('\nRunning toy RNN...\n')

    net_size = 5
    rnn = recurrent_net.RecurrentNeuralNet(net_size)

    for iteration in range(1):
        input_vecs = []
        for i_vec in range(net_size):
            input_vecs.append(utils.one_hot_vec(i_vec, net_size))
        rnn.train(np.array(input_vecs))

    print ('\nToy run complete.\n')