import unittest
import numpy as np
from src import utils


# RNN Gradient Unit Tests

class TestRNNGradients(unittest.TestCase):

    # Test function

    def confirm_almost_eq(self, expected, received, rtol=0.001):
        all_close = np.allclose(expected, received, rtol=0.001)
        if not all_close:
            print ('expected: {0}'.format(expected))
            print ('received: {0}'.format(received))
        self.assertTrue(all_close)


    def confirm_grads(self, in_vec, fn, drv_fn, rtol=0.001):
        delta = 0.001
        slope = (fn(in_vec + delta) - fn(in_vec)) / delta
        drv = drv_fn(in_vec)
        self.confirm_almost_eq(slope, drv, rtol)


    # Gradient tests

    def test_tanh_drv(self):
        in_vec = np.array([1, 2, 4])
        self.confirm_grads(in_vec, utils.tanh, utils.tanh_drv)


    def test_softmax_drv(self):
        delta = 0.001
        in_vec = np.array([1, 2, 4], dtype=float)

        slope = utils.softmax_slope(in_vec)
        drv = utils.softmax_drv(in_vec)
        self.confirm_almost_eq(slope, drv)


# Main method

if __name__ == '__main__':
    unittest.main()