import unittest
import numpy as np
from numpy_rnn import rnn as recurrent_net
from numpy_rnn import utils


# RNN Gradient Unit Tests

class TestRNN(unittest.TestCase):

    # Unit tests

    def test_training(self):
        seq = [0, 1]
        seq_vecs = []
        for val in seq:
            seq_vecs.append(utils.one_hot_vec(val, len(seq)))

        rnn = recurrent_net.RecurrentNeuralNet(len(seq))
        iters = 1

        print ('PRE TRAIN:')
        rnn.pprint()

        for i in range(iters):
            if i % 500 == 0:
                print ('train {0} / {1}'.format(i, iters))
            rnn.train(seq_vecs)

        print ('POST TRAIN:')
        rnn.pprint()

        gen_seq_vecs = rnn.generate_sequence(seq_vecs[0], seq[-1], 10)
        print ('raw generations:')
        print (gen_seq_vecs)
        gen_seq = []
        for vec in gen_seq_vecs:
            gen_seq.append(np.argmax(vec))

        self.assertEqual(seq, gen_seq)


# Main method

if __name__ == '__main__':
    unittest.main()